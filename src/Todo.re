[];

type todo = {
  id: int,
  message: string,
  complete: bool,
};

/* State declaration */
type state = {todos: list(todo)};

/* Action declaration */
type action =
  | Create(string)
  | Complete(int)
  | Delete(int);

let todoId = ref(0);

let newTodo = message => {
  todoId := todoId^ + 1;
  {id: todoId^, complete: false, message};
};

let complete = (id, todos) =>
  List.map(
    todo => todo.id === id ? {...todo, complete: !todo.complete} : todo,
    todos,
  );

let delete = (id, todos) => List.filter(todo => todo.id !== id, todos);

let component = ReasonReact.reducerComponent("Todo");

/* greeting and children are props. `children` isn't used, therefore ignored.
   We ignore it by prepending it with an underscore */
let make = _children => {
  ...component,
  initialState: () => {todos: []},
  /* State transitions */
  reducer: (action, {todos}) =>
    switch (action) {
    | Create(text) =>
      ReasonReact.Update({todos: [newTodo(text), ...todos]})
    | Complete(id) => ReasonReact.Update({todos: complete(id, todos)})
    | Delete(id) => ReasonReact.Update({todos: delete(id, todos)})
    },
  render: ({state, send}) =>
    <div>
      <TodoInput onSubmit={text => send(Create(text))} />
      <div>
        {
          ReasonReact.array(
            Array.of_list(
              List.map(
                todo =>
                  <TodoItem
                    key={string_of_int(todo.id)}
                    message={todo.message}
                    complete={todo.complete}
                    onToggle={_e => send(Complete(todo.id))}
                    clickDelete={() => send(Delete(todo.id))}
                  />,
                state.todos,
              ),
            ),
          )
        }
      </div>
    </div>,
};
