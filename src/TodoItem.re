let component = ReasonReact.statelessComponent("TodoItem");

let make = (~message, ~complete, ~onToggle, ~clickDelete, _children) => {
  ...component,
  render: _self =>
    <div className="item" onClick={_e => onToggle()}>
      <input className="checkbox" type_="checkbox" checked=complete />
      <label> {ReasonReact.string(message)} </label>
      <input
        type_="button"
        className="btn-delete"
        value="x"
        onClick={_e => clickDelete()}
      />
    </div>,
};
