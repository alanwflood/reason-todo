type state = string;

let component = ReasonReact.reducerComponent("TodoInput");

let make = (~onSubmit, _children) => {
  ...component,
  initialState: () => "",
  reducer: (newTodo, _) => ReasonReact.Update(newTodo),
  render: ({state, send}) =>
    <input
      className="input"
      value=state
      type_="text"
      placeholder="What do you want todo?"
      onChange={e => send(ReactEvent.Form.target(e)##value)}
      onKeyDown={
        e =>
          if (ReactEvent.Keyboard.key(e) == "Enter") {
            onSubmit(state);
            send("");
          }
      }
    />,
};
